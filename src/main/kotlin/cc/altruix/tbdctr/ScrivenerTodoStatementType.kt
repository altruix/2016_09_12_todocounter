package cc.altruix.tbdctr

/**
 * Created by pisarenko on 17.08.2016.
 */
enum class ScrivenerTodoStatementType {
    CommentsFile,
    ToDoNodeChild
}
