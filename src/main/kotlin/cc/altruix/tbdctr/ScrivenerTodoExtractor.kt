package cc.altruix.tbdctr

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.apache.commons.compress.archivers.zip.ZipFile
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.w3c.dom.Document
import org.w3c.dom.NodeList
import java.util.*
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

/**
 * Created by pisarenko on 17.08.2016.
 */
class ScrivenerTodoExtractor : IScrivenerTodoExtractor {
    override fun extractToDos(
            dir: File,
            logger: Logger
    ):ScrivenerFileStats {
        val commentTodos:MutableList<ScrivenerTodoStatement> = LinkedList()
        val todoPages:MutableList<ScrivenerTodoStatement> = LinkedList()
// TODO: Continue here. Redefine, how you retrieve the directories and files and how you count them
        zipFile.entries.toList()
                .filter {
                    !it.isDirectory
                            && it.name.toLowerCase().endsWith(".comments")
                }
                .forEach {
                    processCommentsEntry(it, commentTodos, zipFile)
                }

        zipFile.entries.toList()
                .filter {
                    !it.isDirectory && it.name.toLowerCase().endsWith(".scrivx")
                }
                .forEach {
                    processScrivxEntry(it, todoPages, zipFile)
                }

        val result = ArrayList<ScrivenerTodoStatement>(
                commentTodos.size + todoPages.size
        )
        result.addAll(commentTodos)
        result.addAll(todoPages)
        // TODO: Test this
        return result
    }

    private fun processScrivxEntry(
            entry: ZipArchiveEntry,
            target: List<ScrivenerTodoStatement>,
            zipFile: ZipFile
    ) {
        val contents = extractContents(entry, zipFile)
        System.out.println("processScrivxEntry: ${entry.name}")
        System.out.println("CONTENT (start)")
        System.out.println(contents)
        System.out.println("CONTENT (end)")
// TODO: Implement this
// TODO: Test this
    }

    private fun extractContents(entry: ZipArchiveEntry, zipFile: ZipFile): String? {
        val inputStream = zipFile.getInputStream(entry)
        val contents = IOUtils.toString(inputStream)
        return contents
    }

    private fun processCommentsEntry(
            entry: ZipArchiveEntry,
            target: List<ScrivenerTodoStatement>,
            zipFile: ZipFile
    ) {
        val contents = extractContents(entry, zipFile)
        System.out.println("processCommentsEntry: ${entry.name}")

// <?xml version="1.0" encoding="UTF-8"?>
// <Comments Version="1.0">
//     <Comment ID="D5DE2470-55FD-4671-A6CA-39B68E18A96A" Collapsed="No" Color="0.996078 0.964706 0.737255"><![CDATA[{\rtf1\ansi\ansicpg1252\uc1\deff0
// {\fonttbl{\f0\fnil\fcharset204\fprq2 ArialMT;}}
// {\colortbl;\red0\green0\blue0;\red255\green255\blue255;}
// \paperw12240\paperh15840\margl1800\margr1800\margt1440\margb1440\fet2\ftnbj\aenddoc
// \pgnrestart\pgnstarts0
// \pard\plain \ltrch\loch {\f0\fs22\b0\i0 1, 08.08.2016 14:02:16 - TBD, the same smell like in the hospital room, where his father died.}}]]></Comment>
// </Comments>

        val factory = DocumentBuilderFactory.newInstance()
        factory.setNamespaceAware(true)
        val builder: DocumentBuilder
        val xFactory = XPathFactory.newInstance()
        val xpath = xFactory.newXPath()
        val expr: javax.xml.xpath.XPathExpression = xpath.compile("//Comment")
        builder = factory.newDocumentBuilder()
        var doc: Document? = builder.parse(zipFile.getInputStream(entry))

        val nodes = expr.evaluate(doc, XPathConstants.NODESET) as NodeList

        val todoRegex = Regex("""^.*todo[:\\s,].*$""")
        val tbdRegex = Regex(""""^.*tbd[:\\s,].*$""")




        for (i in 0..(nodes.length - 1)) {
            val curNode = nodes.item(i)
            val txt = curNode.textContent.toLowerCase()

            if (txt.matches(todoRegex) || txt.matches(tbdRegex)) { // TODO: Continue here
            //if (txt.contains("todo") || txt.contains("tbd")) {
                System.out.println("curNode: ${curNode.textContent}")
            }

        }


        System.out.println("CONTENT (start)")
        System.out.println(contents)
        System.out.println("CONTENT (end)")

// TODO: Implement this
// TODO: Test this
    }
}
