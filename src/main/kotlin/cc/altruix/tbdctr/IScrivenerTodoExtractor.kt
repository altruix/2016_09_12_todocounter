package cc.altruix.tbdctr

import org.apache.commons.compress.archivers.zip.ZipFile
import org.slf4j.Logger

/**
 * Created by pisarenko on 17.08.2016.
 */
interface IScrivenerTodoExtractor {
    fun extractToDos(
            dir:ZipFile,
            logger:Logger
    ):ScrivenerFileStats 
}
