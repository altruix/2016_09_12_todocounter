package cc.altruix.tbdctr

data class ScrivenerFileStats(
	val comments:Integer,
	val todos:Integer
)
