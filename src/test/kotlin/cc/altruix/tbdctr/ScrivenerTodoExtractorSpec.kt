package cc.altruix.tbdctr

import cc.altruix.mock
import org.apache.commons.compress.archivers.zip.ZipFile
import org.fest.assertions.Assertions
import org.jetbrains.spek.api.Spek
import org.slf4j.Logger
import java.io.File

/**
 * Created by pisarenko on 17.08.2016.
 */
class ScrivenerTodoExtractorSpec : Spek({
    describe("ScrivenerTodoExtractor") {
        val sut = ScrivenerTodoExtractor()
        given("2016_08_17_shortStory1Draft2.scriv.zip") {
            val file = File(
                    "src/test/resources/2016_08_17_shortStory1Draft2.scriv.zip"
            )
            val logger: Logger = mock()
            on("extractToDos") {
                val res = sut.extractToDos(
                        ZipFile(file),
                        logger
                )
                it("should find TODO statements in the comments") {
                    val commentToDos = res
                            .filter {
                                it.type == ScrivenerTodoStatementType.CommentsFile
                            }
                            .toList()
                    Assertions.assertThat(commentToDos).isNotEmpty
                    // TODO: Implement this
                }
                it("should find TODO pages grouped "+
                        "under a page with that title") {
                    // TODO: Implement this
                    val toDoPages = res
                            .filter {
                                it.type == ScrivenerTodoStatementType.ToDoNodeChild
                            }
                            .toList()
                    Assertions.assertThat(toDoPages).isNotEmpty
                }
            }
        }
    }
})
